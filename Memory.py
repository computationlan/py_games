#!/usr/bin/env python2

# implementation of card game - Memory

import simpleguitk as simplegui
import random

# helper function to initialize globals
def new_game():
    global lst, cards, exposed, state, turns
    exposed = []
    cards = []
    lst = []
    for i in range(8):
        lst.append(i)
    cards = lst*2
    random.shuffle(cards) 
    state = 0
    turns = 0
    for i in range(16):
        exposed.append(False)   
         
# define event handlers
def mouseclick(pos):
    global exposed, state, compare_1, compare_2, turns
    index = pos[0]//50
    if exposed[index] == False:
        exposed[index] = True
        if state == 0: 
            state = 1 
            turns += 1
        elif state == 1:
            state = 2
            compare_2 = compare_1
        else:
            state = 1
            turns += 1
            if cards[compare_2] != cards[compare_1]:
                exposed[compare_2] = False
                exposed[compare_1] = False    
        compare_1 = index        
      
# cards are logically 50x100 pixels in size    
def draw(canvas):
    message = "Turns = " + str(turns) 
    for card in range(16):
        card_pos = 50*card + 15
        card_x1 = 50*card
        card_x2 = 50*card + 50
        if exposed[card] == True:
            canvas.draw_text(str(cards[card]), [card_pos, 75], 60, "White")
        else:
            canvas.draw_polygon([[card_x1,0], [card_x2, 0], [card_x2, 100], [card_x1, 100]], 2, "Black", "Green")
    label.set_text(str(message))
    
# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Reset", new_game)
label = frame.add_label("Turns = 0")

# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)

# get things rolling
new_game()
frame.start()