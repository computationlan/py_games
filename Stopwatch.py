#!/usr/bin/env python2

# "Stopwatch: The Game"

import simpleguitk as simplegui
import time

# define global variables
n = 0
count = 0
success = 0
timer_running = False

# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(n):
    A = int(n/600)
    D = n % 10 
    BC = int((n - 600*A - D)/10)
    C = BC % 10
    B = (BC - C)/10
    return str(A) + ":" + str(B) + str(C) + "." + str(D)
    
# define event handlers for buttons; "Start", "Stop", "Reset"

def start_handler():
    global timer_running
    timer.start()
    timer_running = True
    
def stop_handler():
    global count 
    global success
    global timer_running
    timer.stop()
    if timer_running:
        count = count + 1
    if (n % 10 == 0) and timer_running:
        success = success + 1
    timer_running = False
    
def reset_handler():
    global timer_running
    global n
    global count
    global success
    n = 0
    timer_running = False
    timer.stop()
    count = 0
    success = 0
    

# define event handler for timer with 0.1 sec interval
def timer_handler():
    global n
    n  = n + 1

# define draw handler
def draw(canvas):
    message = format(n)
    message2 = str(success) + "/" + str(count)
    canvas.draw_text(message, [100, 100], 36, "White")
    canvas.draw_text(message2, [225, 50], 28, "Green")
    
# create frame
frame = simplegui.create_frame("Home", 300, 200)
frame.add_button("Start", start_handler)
frame.add_button("Stop", stop_handler)
frame.add_button("Reset", reset_handler)
frame.add_label("Try to hit a round number!")

# register event handlers

frame.set_draw_handler(draw)
timer = simplegui.create_timer(100, timer_handler)

# start frame
frame.start()


