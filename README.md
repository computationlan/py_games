# This is a selection of mini-games written in Python as assignments for An Introduction to Interactive Programming in Python (offered by Rice University on Coursera)

Short description of these games (in order of increasing programming complexity):

* **Rock-paper-scissors-lizard-Spock (Rpsls)**
*[A variation of Rock–paper–scissors](https://en.wikipedia.org/wiki/Rock–paper–scissors)* 
* **Guess the Number**
*Guess a secret number selected by the computer*
*You can choose to guess a number between 0 and 100 (not including 100) in no more than 7 attempts or a number between 0 and 1000 (not including 1000) in no more than 10 attempts*
* **Stopwatch**
*Try to stop the stopwatch on the whole second!*
* **Pong**
*[A Python version of one of the first video games](https://en.wikipedia.org/wiki/Pong)*
* **Memory**
*[Put your attention to the test!](https://en.wikipedia.org/wiki/Concentration_(game))* 
* **Yahtzee**
*[Roll the dice!](https://en.wikipedia.org/wiki/Yahtzee)*