#!/usr/bin/env python2

"""
Planner for Yahtzee
Simplifications:  only allow discard and roll, only score against upper level
"""

def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length.
    """
    
    answer_set = set([()])
    for dummy_idx in range(length):
        temp_set = set()
        for partial_sequence in answer_set:
            for item in outcomes:
                new_sequence = list(partial_sequence)
                new_sequence.append(item)
                temp_set.add(tuple(new_sequence))
        answer_set = temp_set
    return answer_set


def score(hand):
    """
    Compute the maximal score for a Yahtzee hand according to the
    upper section of the Yahtzee score card.

    hand: full yahtzee hand

    Returns an integer score 
    """
    unique_values = set(hand)
    result = dict((dummy, 0) for dummy in unique_values)
    for dummy_number in hand:
        result[dummy_number] += 1
    final_result = 0
    for dummy_key in result:
        if dummy_key*result[dummy_key] > final_result:
            final_result = dummy_key*result[dummy_key]    
    return final_result


def expected_value(held_dice, num_die_sides, num_free_dice):
    """
    Compute the expected value based on held_dice given that there
    are num_free_dice to be rolled, each with num_die_sides.

    held_dice: dice that you will hold
    num_die_sides: number of sides on each die
    num_free_dice: number of dice to be rolled

    Returns a floating point expected value
    """
    outcomes = [dummy_number for dummy_number in range(1, num_die_sides+1)]
    expected_outcomes = gen_all_sequences(outcomes, num_free_dice)
    sum_values = 0.0
    count_values = 0.0
    for dummy_number in expected_outcomes:
        sum_values += score(tuple(dummy_number) + held_dice)
        count_values += 1
    return float(sum_values/count_values)


def gen_all_holds(hand):
    """
    Generate all possible choices of dice from hand to hold.

    hand: full yahtzee hand

    Returns a set of tuples, where each tuple is dice to hold
    """
    result = set([()])
    for dummy_i in hand:
        temp_set = set()
        for member in result:
            temp_set.add(member + (dummy_i,))
        result.update(temp_set)
       
    return result

def strategy(hand, num_die_sides):
    """
    Compute the hold that maximizes the expected value when the
    discarded dice are rolled.

    hand: full yahtzee hand
    num_die_sides: number of sides on each die

    Returns a tuple where the first element is the expected score and
    the second element is a tuple of the dice to hold
    """
    cycle_hand = gen_all_holds(hand)
    max_value = 0.0
    result_tuple = tuple()  
    for dummy_tuple in cycle_hand:
        compare_value = expected_value(dummy_tuple, num_die_sides, (len(hand)-len(dummy_tuple)))
        if compare_value > max_value:
            max_value = compare_value
            result_tuple = dummy_tuple
     
    return (max_value, result_tuple)


def run_example():
    """
    Compute the dice to hold and expected score for an example hand
    """
    num_die_sides = 6
    hand = (1, 1, 1, 5, 6)
    hand_score, hold = strategy(hand, num_die_sides)
    print "Best strategy for hand", hand, "is to hold", hold, "with expected score", hand_score
    
    
run_example()
