#!/usr/bin/env python2

# Implementation of classic arcade game Pong

import simpleguitk as simplegui
import random

# initialize globals - pos and vel encode vertical info for paddles
WIDTH = 600
HEIGHT = 400       
BALL_RADIUS = 20
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True
ball_pos = [WIDTH/2, HEIGHT/2]
ball_vel = [0, 0]
paddle1_pos = [[0, HEIGHT/2-PAD_HEIGHT/2], [PAD_WIDTH, HEIGHT/2-PAD_HEIGHT/2], [PAD_WIDTH, HEIGHT/2+PAD_HEIGHT/2], [0, HEIGHT/2+PAD_HEIGHT/2]]
paddle2_pos = [[WIDTH-PAD_WIDTH, HEIGHT/2-PAD_HEIGHT/2], [WIDTH, HEIGHT/2-PAD_HEIGHT/2], [WIDTH, HEIGHT/2+PAD_HEIGHT/2], [WIDTH-PAD_WIDTH, HEIGHT/2+PAD_HEIGHT/2]]
paddle1_vel = 0
paddle2_vel = 0

# initialize ball_pos and ball_vel for new bal in middle of table
# if direction is RIGHT, the ball's velocity is upper right, else upper left
def spawn_ball(direction):
    global ball_pos, ball_vel # these are vectors stored as lists
    ball_pos = [WIDTH/2, HEIGHT/2]
    if direction == RIGHT:
        ball_vel = [random.randrange(120, 240), random.randrange(-180, -60)]
    if direction == LEFT:
        ball_vel = [random.randrange(-240, -120), random.randrange(-180, -60)]
        
# define event handlers
def new_game():
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are numbers
    global score1, score2  # these are ints
    spawn_ball(LEFT)
    score1 = 0
    score2 = 0

def draw(canvas):
    global score1, score2, paddle1_pos, paddle2_pos, ball_pos, ball_vel, paddle1_vel, paddle2_vel
    
    # draw mid line and gutters
    canvas.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White")
    canvas.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White")
    canvas.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White")
        
    # update ball
    if ball_pos[1] >= HEIGHT-BALL_RADIUS:
        ball_vel[1] = -ball_vel[1]
    if ball_pos[1] <= BALL_RADIUS:
        ball_vel[1] = -ball_vel[1]
    if ball_pos[0] <= PAD_WIDTH+BALL_RADIUS and ball_pos[1] >= paddle1_pos[1] [1] and ball_pos[1] <= paddle1_pos[2] [1]:
        ball_vel[0] = -1.1*ball_vel[0]
        ball_vel[1] = 1.1*ball_vel[1]
    elif ball_pos[0] <= PAD_WIDTH+BALL_RADIUS:
        spawn_ball(RIGHT)
        score2 += 1
    if ball_pos[0] >= WIDTH - PAD_WIDTH - BALL_RADIUS and ball_pos[1] >= paddle2_pos[1] [1] and ball_pos[1] <= paddle2_pos[2] [1]:
        ball_vel[0] = -1.1*ball_vel[0]
        ball_vel[1] = 1.1*ball_vel[1]
    elif ball_pos[0] >= WIDTH - PAD_WIDTH - BALL_RADIUS:
        spawn_ball(LEFT)
        score1 += 1
       
    ball_pos[0] += ball_vel[0]/60
    ball_pos[1] += ball_vel[1]/60
            
    # draw ball
    canvas.draw_circle(ball_pos, BALL_RADIUS, 2, "White", "White")
     
    # update paddle's vertical position, keep paddle on the screen    
    paddle1_pos[0] [1] += paddle1_vel/60
    paddle1_pos[1] [1] += paddle1_vel/60
    paddle1_pos[2] [1] += paddle1_vel/60
    paddle1_pos[3] [1] += paddle1_vel/60
    paddle2_pos[0] [1] += paddle2_vel/60
    paddle2_pos[1] [1] += paddle2_vel/60
    paddle2_pos[2] [1] += paddle2_vel/60
    paddle2_pos[3] [1] += paddle2_vel/60
    
    if paddle1_pos[2] [1] >= HEIGHT:
        paddle1_pos = [[0, HEIGHT-PAD_HEIGHT], [PAD_WIDTH, HEIGHT-PAD_HEIGHT], [PAD_WIDTH, HEIGHT], [0, HEIGHT]]
    elif paddle1_pos[0] [1] <= 0:
        paddle1_pos = [[0, 0], [PAD_WIDTH, 0], [PAD_WIDTH, PAD_HEIGHT], [0, PAD_HEIGHT]]
    
    if paddle2_pos[2] [1] >= HEIGHT:
        paddle2_pos = [[WIDTH-PAD_WIDTH, HEIGHT-PAD_HEIGHT], [WIDTH, HEIGHT-PAD_HEIGHT], [WIDTH, HEIGHT], [WIDTH-PAD_WIDTH, HEIGHT]]
    elif paddle2_pos[0] [1] <= 0:
        paddle2_pos = [[WIDTH-PAD_WIDTH, 0], [WIDTH, 0], [WIDTH, PAD_HEIGHT], [WIDTH-PAD_WIDTH, PAD_HEIGHT]]
    
    # draw paddles  
    canvas.draw_polygon(paddle1_pos, 2, "White", "White")
    canvas.draw_polygon(paddle2_pos, 2, "White", "White")
    
    # draw scores
    canvas.draw_text(str(score1), [150, 50], 36, "White")
    canvas.draw_text(str(score2), [450, 50], 36, "White")
        
def keydown(key):
    global paddle1_vel, paddle2_vel
    vel = 200
    if key == simplegui.KEY_MAP["w"]:
        paddle1_vel -= vel 
    if key == simplegui.KEY_MAP["s"]:
        paddle1_vel += vel 
    if key == simplegui.KEY_MAP["up"]:
        paddle2_vel -= vel 
    if key == simplegui.KEY_MAP["down"]:
        paddle2_vel += vel 
        
def keyup(key):
    global paddle1_vel, paddle2_vel
    vel = 200
    if key == simplegui.KEY_MAP["w"]:
        paddle1_vel = 0 
    if key == simplegui.KEY_MAP["s"]:
        paddle1_vel = 0
    if key == simplegui.KEY_MAP["up"]:
        paddle2_vel = 0 
    if key == simplegui.KEY_MAP["down"]:
        paddle2_vel = 0 

def reset_handler():
    new_game()
            
# create frame
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT)
frame.set_draw_handler(draw)
frame.set_keydown_handler(keydown)
frame.set_keyup_handler(keyup)
frame.add_button("Reset the game", reset_handler)

# start frame
new_game()
frame.start()
