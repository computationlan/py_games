#!/usr/bin/env python2

# "Guess the number" mini-project
# input will come from buttons and an input field
# all output for the game will be printed in the console

import simpleguitk as simplegui
import random
import math

n = 7
num_range = 100

# helper function to start and restart the game
def new_game():
    global n
    global secret_number
    # initialize global variables used in your code here
    print "New game. Range is from 0 to", num_range
    if num_range == 100:
        n = 7
    else:
        n = 10
    print "Number of remaining guesses is", n
    secret_number = random.randrange(0, num_range)
    
# define event handlers for control panel
def range100():
    # button that changes the range to [0,100) and starts a new game 
    global num_range
    num_range = 100
    new_game()
    
def range1000():
    # button that changes the range to [0,1000) and starts a new game     
    global num_range
    num_range = 1000
    new_game()
   
def input_guess(guess):
    # main game logic goes here	
    global n
    print ""
    guess_number = int(guess)
    print "Guess was", guess
    n = n - 1
    print "Number of remaining guesses is", n 
    if guess_number < secret_number:
        print "Higher!"
    elif guess_number > secret_number:
        print "Lower!"
    elif guess_number == secret_number:
        print "Correct!" 
        print ""
        new_game()
    if n == 0: 
        print "You have used up all your guesses! The number was", secret_number
        new_game()
    
# create frame
f = simplegui.create_frame("Guess the number", 200, 200)

# register event handlers for control elements and start frame
f.add_button("Range is [0, 100)", range100, 200)
f.add_button("Range is [0, 1000)", range1000, 200)
f.add_input("Enter a guess", input_guess, 200)

f.start()
 
new_game()
